<?php

/**
 * @file
 * Plugin to provide access control based on browscap_get_browser().
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Context is active'),
  'description' => t('Is context active.'),
  'callback' => 'browscap_context_access_ctools_access_check',
  'default' => array('negate' => 0),
  'settings form' => 'browscap_context_access_ctools_access_settings',
  'summary' => 'browscap_context_access_ctools_access_summary',
);

/**
 * Settings form for the 'Is context active' access plugin
 */
function browscap_context_access_ctools_access_settings($form, &$form_state, $conf) {
  // Get list of available contexts.
  $options = array();
  foreach (context_enabled_contexts() as $context) {
    $options[$context->name] = $context->name;
  }
  $form['settings']['active_context'] = array(
    '#type' => 'select',
    '#title' => t('Context'),
    '#description' => t('Select the context that should be active.'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $conf['active_context'],
  );

  return $form;
}

/**
 * Check for access.
 */
function browscap_context_access_ctools_access_check($conf, $context) {
  if (empty($conf['active_context'])) {
    return FALSE;
  }
  $active_contexts = context_active_contexts();
  return isset($active_contexts[$conf['active_context']]);
}

/**
 * Provide a summary description based upon the checked terms.
 */
function browscap_context_access_ctools_access_summary($conf, $context) {
  return t('Context "!name" is active', array('!name' => $conf['active_context']));
}
